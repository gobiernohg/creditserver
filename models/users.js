const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const UserSchema = new Schema({
  name:String,
  tel:String,
  credits:[],
});
const ModeUserClass = mongoose.model('userData', UserSchema);
module.exports = ModeUserClass;