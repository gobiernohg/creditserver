const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const router = require('./routes');
const mongoose = require('mongoose');
const app = express()
app.use(bodyParser.json({ type: '*/*' }));

_statusDb = (err, res) => {
  if(err) console.log("Error: Al connectar a la base de datos" + err);
	else console.log("Connectado a la base");
};

mongoose.connect("mongodb://localhost/credits", _statusDb);
router(app)

const server = http.createServer(app)
const PORT = 3000
server.listen(PORT, () => console.log(`server running in http://localhost:${PORT}`))