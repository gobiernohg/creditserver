
const jwt = require('jsonwebtoken')
const config = require('../config')

const express = require('express')
const protected = express.Router(); 

const GetAllUsers = require('../controllers/GetAllUsersController')
const AddUser = require('../controllers/AddUserController')
const DeleteUser = require('../controllers/DeleteUserController')
const AddCredit = require('../controllers/AddCreditController')
const StatusCredit = require('../controllers/StatusCreditController')
const GetUser = require('../controllers/GetUserDataController')

module.exports = function(app){

	app.set('key', config.key);

	protected.use((req, res, next) => {
    	const token = req.headers['access-token'];
	    if (token) {
	      jwt.verify(token, app.get('key'), (err, decoded) => {      
	        if (err) {
	          return res.json({ mensaje: 'Token invalid' });    
	        } else {
	          req.decoded = decoded;    
	          next();
	        }
	      });
	    } else {
	      res.send({ 
	          mensaje: 'not have token.' 
	      });
	    }
 	});

	 app.get('/', function(req, res){
	   res.json({ online: true });
	 });

	 app.post('/signin', (req, res) => {
	    if(req.body.user === "alex" && req.body.password === "mercer") {
		  const payload = { check:  true};
		  const token = jwt.sign(payload, app.get('key'), {
		   expiresIn: 500000
		  });
		  res.json({
		   mensaje: 'Bienvenido',
			 token: token,
			 error:false
		  });
	    } else {
	        res.json({ mensaje: "Usuario o contraseña incorrectos", error:true})
	    }
	})

	app.post('/getAllUsers', protected, GetAllUsers.getAllUsers);
	app.post('/addUser', protected, AddUser.saveUser);
	app.post('/deleteUser', protected, DeleteUser.deleteUser);
	app.post('/addCredit', protected, AddCredit.addCredit);
	app.post('/updateStatusCredit', protected, StatusCredit.statusCredit);
	app.post('/getUser', protected, GetUser.getUser);
	
};