# SERVER

# Instalación

Utiliza el gestor de paquetes  [npm](https://www.npmjs.com/get-npm) para instalar las dependencias.

```bash
npm install
```

# Endpoints
## Main URL 

http://localhost:300

### Login
#### URL: /signin
#### Method: POST
#### Body
```json
{
    "user": 'alex',
    "password": 'mercer',
}
```
#### Response
```json
{
    "mensaje": 'Bienvenido',
    "token": 'token',
    "error":false
}
```

### Get All Users

#### URL: /getAllUsers
#### Method: POST
#### Body

```json
'headers':{
    'acces-token': 'token'
},
body{}
```

#### Response

```json
[
    {
       "_id" :"5ecc807288587613630af156",
        "credits" : [
            {
                "monto" : "$22.00",
                "status" : "dispensado",
                "idCredit" : 1590465192246
            }
        ],
        "name" : "Alex Mercer",
        "tel" : "7712079087",
        "__v" : number
    },
    ...
]
```

### Add User

#### URL: /addUser
#### Method: POST
#### Body

```json
'headers':{
    'acces-token': 'token'
},
body:{
   "name": "Alex",
   "tel": "7712079079",
}
```

#### Response

```json
{
   "_id" :"5ecc807288587613630af156",
    "credits" : [],
    "name" : "Alex",
    "tel" : "7712079079",
    "__v" : number
},
```

### Delete User

#### URL: /deleteUser
#### Method: POST
#### Body

```json
'headers':{
    'acces-token': 'token'
},
body:{
   "_id": ""5ecc807288587613630af156""
}
```

#### Response

```json
{
  'action' : true, 
  'error': true
}
```

### Get User

#### URL: /getUser
#### Method: POST
#### Body

```json
'headers':{
    'acces-token': 'token'
},
'body':{
   "_id": ""5ecc807288587613630af156""
}
```

#### Response

```json
{
   "_id" :"5ecc807288587613630af156",
    "credits" : [],
    "name" : "Alex",
    "tel" : "7712079079",
    "__v" : number
},
```

### Add Credit

#### URL: /addCredit
#### Method: POST
#### Body

```json
'headers':{
    'acces-token': 'token'
},
'body':{
   "_id": 5ecc807288587613630af156"
    "monto":'$500,000.00'
    "status":en_proceso
}
```

#### Response

```json
{
   "error": false
}
```

### Update Credit Status

#### URL: /updateStatusCredit
#### Method: POST
#### Body

```json
'headers':{
    'acces-token': 'token'
},
'body':{
   "_id": 5ecc807288587613630af156"
    "idCredit":'1590465192246'
    "status":'cancelado'
}
```

#### Response

```json
{
   'message':'Se a cambiado el status del credito', 
   'error':false
}
```