const  UserDataSchema = require('../models/users');

exports.statusCredit = function(req, res, next) {
  UserDataSchema.updateOne({
    _id:req.body._id, 
    'credits.idCredit':req.body.idCredit
  },
  {
    $set:{"credits.$.status":req.body.status}
  }, function(err, existUser) {

    if(err){
      return next({message:'Ocurrio un error', error:true});
    }
    res.json({message:'Se a cambiado el status del credito', error:false})
  });
};
