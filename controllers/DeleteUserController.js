const  UserDataSchema = require('../models/users');

exports.deleteUser = function(req, res, next) {
  UserDataSchema.deleteOne({ _id : req.body._id }, function(err, deleteAction) {
    if(err){
      return res({action:"No se puede borrar el usuario", error: true});
    }
    if(deleteAction){
      return res.json({action : true, error: false})
    }else{
      return res.json({action : "No se puede borrar el usuario", error: true})
    }
    
  });
};


