const  UserDataSchema = require('../models/users');

exports.addCredit = function(req, res, next) {
  UserDataSchema.findOne({_id:req.body._id}, function(err, existUser) {
    if(err){
      return next(err);
    }
    if(existUser){
      const credits=existUser.credits;
      credits.push({
        monto:req.body.monto,
        status:req.body.status, 
        idCredit: Number(new Date())
      })
      existUser.credits= credits;
      existUser.save((err) => {
        if(err){
          return res.json({error:true}) 
        }
        res.json(existUser)
      });
    }else{
      return res.json({error:true}) 
    }
  });
};

