const  UserDataSchema = require('../models/users');

exports.getUser=  function(req, res, next) {

  UserDataSchema.findOne( {_id: req.body._id}, function ( err, existUser ) {
    if (err) {
      return next( { user: {}, error:true } );
    }
    if (existUser) {
      return res.json( { user: existUser, error:false} )
    } else {
      return res.status( 422 ).send( { user: {}, error:true } );
    }
  } );
};