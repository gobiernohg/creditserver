const  UserDataSchema = require('../models/users');

exports.getAllUsers=  function(req, res, next) {

  UserDataSchema.find( {}, function ( err, existUsers ) {
    if (err) {
      return next( { users: [] } );
    }
    if (existUsers) {
      return res.json( { users : existUsers} )
    } else {
      return res.status( 422 ).send( { users: [] } );
    }
  } );
};